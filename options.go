package main

import (
	"encoding/json"
	"log"
	"os"
	"time"
)

type Options struct {
	TotalMinutes   int
	DelayInSeconds int
	PathToCLI      string
}

func (o Options) TotalTicks() int {
	return o.TotalMinutes * 60 / o.DelayInSeconds
}

func (o Options) DelayDuration() time.Duration {
	return time.Duration(o.DelayInSeconds) * time.Second
}

func MustReadOptions() *Options {
	file, err := os.ReadFile("mcsm-monitor.json")

	if err != nil {
		log.Fatal(err.Error())
	}

	opts := new(Options)
	err = json.Unmarshal(file, opts)

	if err != nil {
		log.Fatal(err.Error())
	}

	return opts
}
