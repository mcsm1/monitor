package main

import (
	"fmt"
	"os"
	"os/exec"
	"time"
)

func main() {
	opts := MustReadOptions()
	ticks := 0

	for {
		time.Sleep(opts.DelayDuration())
		ticks++

		stdout, err := exec.Command(opts.PathToCLI, "isActive").Output()

		if err != nil {
			logError(err)
			ticks = 0
			continue
		}

		if string(stdout) == "true" {
			ticks = 0
			continue
		}

		if ticks < opts.TotalTicks() {
			continue
		}

		err = exec.Command(opts.PathToCLI, "shutdown").Run()

		if err != nil {
			logError(err)
		}
	}
}

func logError(e error) {
	flag := os.O_CREATE | os.O_WRONLY | os.O_APPEND
	file, err := os.OpenFile("mcsm-monitor.log", flag, 0644)

	if err != nil {
		return
	}
	defer file.Close()

	t := time.Now().Format("2006-01-02 15:04:05")
	msg := fmt.Sprintf("%s\n%s\n\n", t, e.Error())
	file.WriteString(msg)
}
