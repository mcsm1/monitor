# MCSM Monitor

This software is a component of the
[Minecraft Server Manager](https://gitlab.com/mcsm1) pack.
See that page for more details.

Its purpose is to shutdown the computer that the server is running on.
The CLI tool will start the monitor once the computer launches, and it will
continue to run perpetually in the background.

If there are errors, it may create a `mcsm-monitor.log` file.

# Customization
The program expects a `mcsm-monitor.json` file next to the executable.
This file is used for customization and must be formatted as such:

```json
{
    "TotalMinutes": 10,
    "DelayInSeconds": 30,
    "PathToCLI": "./mcsm-cli"
}
```

`TotalMinutes` - int - the number of minutes

`DelayInSeconds` - int - the number of seconds the software will sleep for
in between checks to the CLI tool

`PathToCLI` - string - the path to the CLI tool
